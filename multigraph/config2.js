var oldCallback;

// [1] Add Asset Id
var tempSensorId = '5466770ee1361b04407f6598';
var soundSensorId = '5471d32c85915b09b40c4bdd';
var airqSensorId = '547385ca85915b09b40c4c0c';
var lightSensorId = '5471d33985915b09b40c4bde';
var attentionSensorId = '5471d36c85915b09b40c4be0';

// [2] Create data array
var tempData = [];
var soundData = [];
var airqData = [];
var lightSensorData = [];
var attentionData = [];

// [3] Add asset id to the array
var sensors = [];
sensors.push(tempSensorId);
sensors.push(soundSensorId);
sensors.push(airqSensorId);
sensors.push(lightSensorId);
sensors.push(attentionSensorId);

function getTopic(clientId, assetId) {
  return "client." + clientId + ".out.asset." + assetId + ".state";
}

init(function callback() {
  console.log('Init done');

  oldCallback = window.client.connectCallback;
  window.client.connectCallback = connectToSensors;
});

function connectToSensors() {

  //Subscribe to our stuff
  for (var i = 0; i < sensors.length; i++) {
    var assetId = sensors[i];

    var topic = getTopic(window.clientId, assetId);
    console.log('topic', topic);

    window.client.subscribe('/exchange/root/' + topic,   update, { id: assetId });
  }

  oldCallback();
  setUpChart();
}

// [4] - Add case for asset id
function update(message) {
  console.log('Got update', message.body, 'from', message.headers.subscription);

  var val = message.body.split('|')[1];
  switch(message.headers.subscription) {

    case tempSensorId:
      tempData.push(val);
      break;

    case soundSensorId:
      soundData.push(val);
      break;

    case airqSensorId:
      airqData.push(val);
      break;

    case lightSensorId:
      lightSensorData.push(val);
      break;

    case attentionSensorId:
      attentionData.push(val);
      break;

    default:
      console.log('No asset found', message.body);
      break;
  }
}

function setUpChart() {
  Highcharts.setOptions({
    global: {
      useUTC: false
    }
  });

  $('#container').highcharts({
    credits: {
      enabled: false
    },
    chart: {
      type: 'spline',
      marginRight: 10,
      events: {
        load: function () {

          // [5] Add series id
          var soundSeries = this.series[0];
          var tempSeries = this.series[1];
          var airqSeries = this.series[2];
          var lightSeries = this.series[3];
          var attentionSeries = this.series[4];

          setInterval(function () {

            // [6] Add variable
            var x = (new Date()).getTime(), // current time
              ytemp = tempData[tempData.length-1] ? parseInt(tempData[tempData.length-1]) : 0,
              ysound = soundData[soundData.length-1] ? parseInt(soundData[soundData.length-1]) : 0,
              yairq = airqData[airqData.length-1] ? parseInt(airqData[airqData.length-1]) : 0,
              ylight = lightSensorData[lightSensorData.length-1] ? parseInt(lightSensorData[lightSensorData.length-1]) : 0,
              yattention = attentionData[attentionData.length-1] ? parseInt(attentionData[attentionData.length-1]) : 0;

            tempSeries.addPoint([x, ytemp], false, true);

            airqSeries.addPoint([x, yairq], false, true);

            lightSeries.addPoint([x, ylight], false, true);

            attentionSeries.addPoint([x, yattention], false, true);

            // [7] Add point here

            // This should be the last one as it redraws the chart
            soundSeries.addPoint([x, ysound], true, true);
          }, 1000);
        }
      }
    },
    title: {
      text: 'Black Box'
    },
    xAxis: {
      type: 'datetime',
      tickPixelInterval: 150
    },
    yAxis: {
      title: {
        text: 'Value'
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: '#808080'
      }],
      floor: 0
    },
    legend: {
      enabled: true
    },
    exporting: {
      enabled: false
    },
    // [8] Add series with a title and dummy data
    series: [{
      name: 'Sound Sensor',
      data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: 0
                        });
                    }
                    return data;
                }())
    },{
      name: 'Temperature Sensor',
      data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: 0
                        });
                    }
                    return data;
                }())
    }, {
      name: 'Air Quality Sensor',
      data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: 0
                        });
                    }
                    return data;
                }())
    },{
      name: 'Light Sensor',
      data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: 0
                        });
                    }
                    return data;
                }())
    },{
      name: 'Attention Level',
      data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: 0
                        });
                    }
                    return data;
                }())
    }]
  });
}