/*

	We have to many parts to configure

	receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	userInputCommandAction() - What action must be taken when user input commands are made from the UI

	3 Steps to confiure your widget
	- [1] Specify valadation for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex?)
	- [2] Specify how the data is loaded in to the UI element
	- [3] Specify how user input is caught and processed
*/

// [1] Set up feed data methods
var input = true;
var button = document.getElementById('actuator-button');

var validate = function(value){ 
	return true;
}

// [2] How do you want to update the UI element
button.addEventListener("mousedown", function(){
	userInputCommandAction("true");
	button.innerHTML="ON";
});


button.addEventListener("mouseup", function(){
	userInputCommandAction("false");
	button.innerHTML="OFF";
});

button.addEventListener("mouseout", function(){
	userInputCommandAction("false");
	button.innerHTML="OFF";
});

function uiUpdateEvent(){};

//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	//E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);