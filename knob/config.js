/*
	We have to many parts to configure

	receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	userInputCommandAction() - What action must be taken when user input commands are made from the UI

	3 Steps to confiure your widget:
	[1] Specify valadation for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex?)
	[2] Specify how the data is loaded in to the UI element
	[3] Specify how user input is caught and processed
*/
var min = null;
var max = null;
// [1] Set up feed data methods
var validate = function(value){ 
	// Put in a reg ex, comaparison, or compare against the resource profile
	if(value > -1 || value < 1025){
		$('#knobWidget').val(value).trigger('change'); 
		return true;
	}else{
		 log("This is a knob widget which only allows integer values between 0 and 1024");
		return false;
	}
}

// [2] How do you want to update the UI element
var uiUpdateEvent = function(feedData){
	 //Add your custom logic to update the user interface based from the payload data
		$('#knobWidget').val(feedData).trigger('change'); 
}

//[3] Specify how user input is caught and processed
	//There options here
	//   Set up a listener - E.g   $('#myInput').change(function() {};
	//   Set this as an onclick function on the DOM object - E.go onclick='userInputCommandAction(this)'
	//   Call this function from within widgets custom code
var catchInputEvent = function(){
	$("#knobWidget").knob({
		change : function (input) {
			userInputCommandAction(input);
		}
	});
}

//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	//E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
	var minMax = sendMinMax();
	min = minMax[0];
	max = minMax[1];
	// Primarily deals with drawing the knob and making it responsive to page load (Not responsive by default)
	window.onresize = function(){ 
		location.reload(); 
	}

	var knobWidthHeight;
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;
	if(windowHeight > windowWidth){
		knobWidthHeight = Math.round(windowWidth*0.80);
	}else{
		knobWidthHeight = Math.round(windowHeight*0.80);
	}
	if(min != null || max != null)
	{
		$('#knobWidget').attr('data-min',min).attr('data-max',max);
	}
	$('#knobWidget').attr('data-width',knobWidthHeight).attr('data-height',knobWidthHeight);
	catchInputEvent();
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);