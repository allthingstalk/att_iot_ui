/*

	We have to many parts to configure

	receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	userInputCommandAction() - What action must be taken when user input commands are made from the UI

	3 Steps to confiure your widget
	- [1] Specify valadation for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex?)
	- [2] Specify how the data is loaded in to the UI element
	- [3] Specify how user input is caught and processed
*/

// [1] Set up feed data methods
var validate = function(value){ 
 // Put in a reg ex, comaparison, or compare against the resource profile
 document.getElementById("textarea").innerHTML = value;
	 return true; //No validation!
}

// [2] How do you want to update the UI element
var uiUpdateEvent = function(feedData){
	 //Add your custom logic to update the user interface based from the payload data
	document.getElementById("textarea").innerHTML = feedData;
}

// [3] How do you want to update the UI element
$('#textbox').keypress(function(e) {
		var evt = e || window.event
	if(evt.which == 13) {
			evt.preventDefault(); 
			var input = $('input').val();    
			pub(input)
		}
});

//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	//E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);