/*
	We have to many parts to configure

	receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	userInputCommandAction() - What action must be taken when user input commands are made from the UI

	3 Steps to confiure your widget:
	[1] Specify valadation for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex?)
	[2] Specify how the data is loaded in to the UI element
	[3] Specify how user input is caught and processed
*/

// [1] Set up feed data methods
var validate = function(value){ 
	// Put in a reg ex, comaparison, or compare against the resource profile
	if(value > -1 || value < 1025){
		return true;
	}else{
		 log("This is a knob widget which only allows integer values between 0 and 1024");
		return false;
	}
}

// [2] How do you want to update the UI element
var uiUpdateEvent = function(feedData){
	 //Add your custom logic to update the user interface based from the payload data
		$('#knobWidget').val(feedData).trigger('change'); 
//
//      "meta": {
//        "value":"illuminance",
//        "key":"Surface illuminated by"
//      },
 
luxMap = {
		"dry":0.1,  //Dry soil
		"humid":300,  //Humid soil
		"wet":700     //In water
	};


message ="dry";

$.each(luxMap, function(key,value){
	if(parseInt(feedData,10) > parseInt(value,10)){
			message = key;
		}
});

$('#context').html(message+" soil");
$('#imgHolder').html("<img style='height:60%;'src='"+message+".png' />");

}
//[3] Specify how user input is caught and processed
	//There options here
	//   Set up a listener - E.g   $('#myInput').change(function() {};
	//   Set this as an onclick function on the DOM object - E.go onclick='userInputCommandAction(this)'
	//   Call this function from within widgets custom code

//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){

}

var timestamp = getParam("timestamp");

if(timestamp == "hide"){
	$("#timestamp").hide();
}
//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);