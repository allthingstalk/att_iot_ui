var validate = function(value){ 
  return true;
}


var myRecords = [];
var tableFlag = false;
var count = 0;
var limit = 100;
var historicDataFlag = false;

var uiUpdateEvent = function(feedData, timestamp){
	if(historicDataFlag == false)
	{
		if(tableFlag == false)
		{
			$('.widget').attr('style','display: show;');
			$('#updatecheck').attr('style','display: none;');
			$('#timestamp').attr('style','display: none;');
			$('#container').css("text-align", "left");
			$('#container').css("height", "100%");
			tableFlag = true    
			createTable();
		}
		
		if(isTimeStampRecent(timestamp))
		{
			timestamp = prettyhms(timestamp);
		}else
		{
			timestamp = prettyhms(now());
		}
		
		// insert to json object
		var temp = new Object();
		temp["data"] = feedData;
		temp["time"] = timestamp;
		myRecords.push(temp);
		
		if ( count < limit )
		{
			console.log("feedData ----- " + feedData );
			console.log("TimeStamp ----- " + timestamp );
			//update the dynatable
			dynatableObj.settings.dataset.originalRecords = myRecords;
			dynatableObj.process();
			count++;
		}

		}
}


var dynatableObj;

var createTable = function(){

  dynatableObj= $('#my-table').dynatable({
		dataset: {
			perPageDefault: 10,
			sorts: { 'time': -1 },
			perPageOptions: [10, 20, 30, 40, 50, 100]
		},
		features: {
			paginate: true,
			search: false,
			sort: true,
			recordCount: true
		}
	}).data('dynatable');

}


//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	$('#timestamp').attr('style','display: none;');
	$('#updatecheck').attr('style','display: show;');

  //E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);

var resetCanvas = function(){
  $('#canvas').remove(); // this is my <canvas> element
  $('#widgetcontainer').append('<canvas id="canvas"><canvas>');
}

var showData = function(data){
	historicDataFlag = true;
	historyBucket = data;

    if (historyBucket.data.length == 0)
    {
        $('#updatecheck').attr('style','display: show;');
        document.getElementById('updatecheck').innerHTML = 'No Data Returned';

    }else{

    	$('#loading').attr('style','display: none;');
    	$('#content').attr('style','display: show;');

    	if(tableFlag == false)
        {
            $('.widget').attr('style','display: show;');
            $('#updatecheck').attr('style','display: none;');
            $('#timestamp').attr('style','display: none;');
            $('#container').css("text-align", "left");
            $('#container').css("height", "100%");
            tableFlag = true; 
        }

        for(i=0; i<historyBucket.data.length;i++)
        {
            
            dynatableObj = '';
            createTable();
            // insert to json object
            myRecords = [];
            for(i=0; i<historyBucket.data.length;i++)
            {
                var temp = new Object();
                timestamp = historyBucket.data[i].at;
                timestamp = timestamp.replace('T',' ');
                timestamp = timestamp.replace('Z','');
                temp["data"] = historyBucket.data[i].data;
                temp["time"] = timestamp;
                myRecords.push(temp);
                //update the dynatable
                dynatableObj.settings.dataset.originalRecords = myRecords;
                dynatableObj.process();
            }
        }
    }
}


var telemetricMode = function(){

	historyBucket = [];
	historicDataFlag = false;
	dynatableObj = '';
	createTable();
	myRecords = [];
}