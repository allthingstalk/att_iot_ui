/*

	We have to many parts to configure

	receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	userInputCommandAction() - What action must be taken when user input commands are made from the UI

	3 Steps to confiure your widget
	- [1] Specify valadation for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex?)
	- [2] Specify how the data is loaded in to the UI element
	- [3] Specify how user input is caught and processed
*/

// [1] Set up feed data methods
var validate = function(value){ 
 // Put in a reg ex, comaparison, or compare against the resource profile
	if(value=="false" || value == false || value=="0" || value==0 ){
		return true;
	}else if(value=="true" || value == true || value=="1" || value==1) {
		return true;
	}else{
		log("This isn't a boolean value:" + value);
		return false;
	}
}

// [2] How do you want to update the UI element
var uiUpdateEvent = function(feedData){
	 //Add your custom logic to update the user interface based from the payload data
			if(feedData=="false" || feedData=="0" || feedData==0 ){
				document.getElementById('on').className = "";
				document.getElementById('off').className = "active";
			}else{
				document.getElementById('off').className = "";
				document.getElementById('on').className = "active";
			}

}

// [3] How do you want to update the UI element
$('#imagearea').click(function() {

		if(document.getElementById('off').className == ""){
			 userInputCommandAction("false");
		}
		else{
			userInputCommandAction("true");
		}	
});

//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	//E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);