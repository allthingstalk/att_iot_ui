# WidgIoTs

## Internet of Things Widgets for Idiots.


A collection of really simple widgets for building 'Internet of Things' enabled web pages, dashboards or hybrid mobile apps.

Ok, you're not an idiot, but these widgets are REALY simple, you could make your own with just a few lines of HTML and JS.

## What's on offer

|Widget				|Data type	  |Functionality			|Useful for			| 
|---				|---	  	  |---						|---				| 
| [toggle](http://widget.smartliving.io/toggle/)		 	| boolean 	  | real-time | sensor, actuator, virtual |
| [button](http://widget.smartliving.io/button/) 			| boolean     | real-time | sensor, actuator, virtual |
| [plaintext](http://widget.smartliving.io/plaintext/)  		| string      | real-time | sensor 					  |
| [textbox](http://widget.smartliving.io/textbox/)   		| string      | real-time | sensor, actuator, virtual |
| [textarea](http://widget.smartliving.io/textarea/) 			| string      | real-time | sensor, actuator, virtual |
| [knob](http://widget.smartliving.io/knob/)				| numeric     | real-time | sensor, actuator, virtual |
| [linegraph](http://widget.smartliving.io/linegraph/)		| numeric     | real-time, logger | sensor |	
| [notification](http://widget.smartliving.io/notification/)		| string      | real-time | sensor |
| [Text-to-speech](http://widget.smartliving.io/speech/)	| string      | real-time | sensor |

## What can you use them for?
 - Display data from sensors, web services or any other smart device, instantly
 - Remotely control actuators, smart devices and pretty much anything you connect, with immediate status feedback
 - Pull historical data, from a database where your end points store data or another API
 - Debug your personal 'Internet of Things' with simple widgets you can access from anywhere
 - Create 'virtual' sensors and actuators that exist in your web pages, fully supported for use with automation rules (when-that-then-this) or scripting

## Things which make these widgets possible
 - sockJS - Real-time bi-direcitonal communications using websockets 
 - HTML 5 Device API - Realy cool stuff but not enough browser support to go crazy with these, yet
 - chart.js - Data representation with line, bar or pie
 - dynatable.js - Awesome resposnsive, HTML, searchable table
 - knob.js - Great library for displaying numeric values
 - toggle.css - Lovely CSS toggle from xxx
 - jQuery - yep. 

 ## Roadmap
 - Add some Polymer goodness
 - Highcharts and historic data API calls to SmartLiving
 - Packery Dashboard builder