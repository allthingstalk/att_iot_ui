/*

		▄▄▌ ▐ ▄▌▪  ·▄▄▄▄   ▄▄ • ▪        ▄▄▄▄▄.▄▄ · 
		██· █▌▐███ ██▪ ██ ▐█ ▀ ▪██ ▪     •██  ▐█ ▀. 
		██▪▐█▐▐▌▐█·▐█· ▐█▌▄█ ▀█▄▐█· ▄█▀▄  ▐█.▪▄▀▀▀█▄
		▐█▌██▐█▌▐█▌██. ██ ▐█▄▪▐█▐█▌▐█▌.▐▌ ▐█▌·▐█▄▪▐█
		 ▀▀▀▀ ▀▪▀▀▀▀▀▀▀▀• ·▀▀▀▀ ▀▀▀ ▀█▄▀▪ ▀▀▀  ▀▀▀▀ 

	Brutally hacked with coffee and metal...
	
	Currently refactoring a lovely fluffy developer friendly version.


*/


var libs = '<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script><script src="http://cdnjs.cloudflare.com/ajax/libs/sockjs-client/0.3.4/sockjs.min.js"></script><script src="http://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script><script src="https://code.jquery.com/ui/jquery-ui-1-9-git.js"></script><link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/themes/ui-lightness/jquery-ui.css"/><link href="http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext" rel="stylesheet" type="text/css"><script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>'
document.writeln(libs);

//var brokerUrl="att-2.cloudapp.net", brokerPort=15674;
// ping
var brokerUrl = "broker.smartliving.io",
    brokerPort = 15674;
var url = location.pathname;
var widgetName = url.split('/').pop();
var deviceId = '',
    assetId, type, clientId, clientKey, gatewayId = '';
var debug; // Holds random parameters you specify for debugging
var publishTopic, subscribeTopic;
var source = "/exchange/root/";
var noconfig = '<div id="noconfig"><p>To set up this widget you must pass your resource identifiers in the URL</p></div>';
var min, max, prefix, postfix, timestampDisplay, timestampTextual;

var setupHtml = '<br><div class="form-group"><label class="login-title">Client ID: </label><div class="input-group right"><input type="input" id="id"/></div></div><div class="form-group"><label  class="login-title">Client Key: </label><div class="input-group right"><input type="input" id="key"/></div></div><div class="col"><div class="form-group"><label class="login-title">Asset ID: </label><div class="input-group right"> <input type="input" id="asset"/></div></div><div class="form-group"><label  class="login-title">Device ID: </label><div class="input-group right"><a id="addDevice" onclick="addDevice()">add device</a><input type="input" id="device" style="display:none"/></div></div><div class="form-group"><label id="lblGate" style="display:none;" class="login-title">Gateway ID: </label><div class="input-group right"><input type="input" id="gateway" style="display:none;"/></div></div>  <div class="form-group"><label class="login-title">type : </label><div class="input-group right"><select id="type" style="width: 150px;"><option value="sensor">sensor</option><option value="actuator">actuator</option><option value="virtual-input">virtual input</option><option value="virtual-output">virtual output</option><option value="debug">debug</option></select></div></div></div>';

var configHtml = '<br><div class="form-group"><label class="login-title">Client ID: </label><div class="input-group right"><input type="input" id="id" value="' + getParam("id") + '"/></div></div><div class="form-group"><label  class="login-title">Client Key: </label><div class="input-group right"><input type="input" id="key"  value="' + getParam("key") + '"/></div></div><div class="col"><div class="form-group"><label class="login-title">Asset ID: </label><div class="input-group right"> <input type="input" id="asset"  value="' + getParam("asset") + '"/></div></div><div class="form-group"><label  class="login-title">Device ID: </label><div class="input-group right"><a id="addDevice" onclick="addDevice()">add device</a><input type="input" id="device" style="display:none"  value="' + deviceId + '" /></div></div><div class="form-group"><label id="lblGate" style="display:none;" class="login-title">Gateway ID: </label><div class="input-group right"><input type="input" id="gateway" style="display:none;" value="' + gatewayId + '"/></div></div><div class="form-group"><label class="login-title">type : </label><div class="input-group right"><select id="type" style="width: 150px;"><option value="sensor">sensor</option><option value="actuator">actuator</option><option value="virtual-input">virtual input</option><option value="virtual-output">virtual output</option><option value="debug">debug</option></select></div></div></div>';

var errorHtml = '<br>An error has occured, ensure your id and key are correct.';
var dataErrorHtml = '<br>Invalid Data';

var description, name, lastValue, profile, lastTimestamp;

var historyData = {
    bucket: []
};;



function addDevice(argument) {

    $('#addDevice').attr('style', 'display: none;');
    $('#device').attr('style', 'display: show;');
    $('#lblGate').attr('style', 'display: show;');
    $('#gateway').attr('style', 'display: show;');
}

function init(widget) {
    loading(true);
    if (grabSetupParams()) {
        console.log("[0] SETUP params are present");

        //Let's update the title to help debug when we have lots of tabs open
        document.title = widgetName + " type: " + type + " asset: " + assetId;

        var typeConfigured = true;
        var exchange;

        loadControlSettings();

        //Try to connect client, if ok we'll use the callback
        stomp(error, proceed);

    } else {
        console.log("[0] SETUP invalid parameters received")

        $('body').append(modalForm("Setup your widget", "", setupHtml));
        grabSetupParams()
        $(function() {
            $("#dialog").dialog({

                modal: true,
                draggable: false,
                resizable: false,
                position: 'center',
                show: 'blind',
                hide: 'blind',
                closeOnEscape: false,
                beforeClose: function(event, ui) {
                    return false;
                },
                dialogClass: "noclose",
                buttons: {
                    "Create widget": {
                        id: 'create',
                        text: 'Create widget',
                        click: function() {

                            var device = document.getElementById('device').value;
                            var gateway = document.getElementById('gateway').value;
                            loadParams(device, gateway);

                        },
                        "class": "createWidget"
                    },
                    "?": {
                        id: 'help',
                        text: '?',
                        'title': 'Kindly input the setup for credentials and choose device type',
                        click: function() {
                            $(this).dialog("close");
                        },
                        "class": "helpButton"
                    },
                }
            });
        });
    }
    widget(); //This is the final calls passed into init to do once all the conneciton is setup, typically pass draw commands here
}

function loadControlSettings() {
    //Try local paramers
    //Convient param for debugging brokers
    min = getParam("min");
    max = getParam("max");
    prefix = getParam("prefix");
    postfix = getParam("postfix");
    timestampDisplay = getParam("timestampDisplay");
    timestampTextual = getParam("timestampTextual");
}

//[0] - Get parameters from the URL
function grabSetupParams() {
    gatewayId = getParam("gateway");
    deviceId = getParam("device");
    assetId = getParam("asset");
    clientId = getParam("id");
    clientKey = getParam("key");
    type = getParam("type");

    if (assetId != null && clientId != null && clientKey != null && type != null) {
        //Do some smarter validation here
        return true
    } else {
        return false;
    }
}

//[1] Authentication

var telemetry; // This is the stomp connection object
var topic;

function stomp(error, proceed) {

    vhost = clientId;


    //Convient param for debugging brokers
    var customHost = getParam("customHost");
    if (customHost != null) {
        brokerUrl = customHost;
    }

    //Convient param for debugging vhosts
    var vhostParam = getParam("vhost");
    if (vhostParam != null) {
        vhost = vhostParam;
    }

    //Convient param for debugging exchanges
    var exParam = getParam("exchange");
    if (exParam != null) {
        source = "/exchange/" + exParam;
    }

    //Convient param for debugging exchanges
    var topicParam = getParam("topic");
    if (topicParam != null) {
        topic = topicParam;
    }

    var ws = new SockJS("http://" + brokerUrl + ":" + brokerPort + "/stomp");
    var s = Stomp.over(ws);
    s.heartbeat.outgoing = 5000;
    s.heartbeat.incoming = 0;


    var username = clientId,
        password = clientKey;

    console.log("Connecting to: " + ws)

    s.connect(username, password, function(x) {
        console.log("[1] AUTHENTICATION: Connection successful");
        proceed();

    }, function(data) {
        loading(false);
        error(true)
    }, vhost);

    telemetry = s;

    telemetry.ws.onclose = function(t) {
        return function() {
            var e;
            e = "Whoops! Lost connection " + t.ws.url;

            if (typeof t.debug === "function") {
                t.debug(e)
            }
            t._cleanUp();
            return typeof s === "function" ? s(e) : void 0
        }
    }

    return true;
}

//[2] Check out the device/asset id's are valid and the set up topic for the type specifed
var proceed = function proceed() {
    var assetsData = getAsset();
    if (type == "sensor") {
        console.log("[2] IDENTIFICATION - Type sensor, topics configured")
        typeDescription = "This will display feed data sent from a sensor. User input disabled."

        disableUI()

        subscribeTopic = "client." + clientId + ".in.asset." + assetId + ".state"

    } else if (type == "actuator") {
        console.log("[2] IDENTIFICATION - Type actuator, topics configured")
        typeDescription = "This will allow you to send actuator commands, and it will also react to the actuator state changes triggered from other actors. User input allowed"

        subscribeTopic = "client." + clientId + ".in.asset." + assetId + ".state"

        if (gatewayId != null && deviceId != null) {
            publishTopic = "client." + clientId + ".in.gateway." + gatewayId + ".device." + deviceId + ".asset." + assetId + ".command"
        } else if (deviceId != null) {
            publishTopic = "client." + clientId + ".in.device." + deviceId + ".asset." + assetId + ".command"
        } else {
            publishTopic = "client." + clientId + ".in.asset." + assetId + ".command"
        }

    } else if (type == "virtual-input") {
        console.log("[2] IDENTIFICATION - Type vinput, topics configured")

        publishTopic = "client." + clientId + ".out.asset." + assetId + ".state"

        typeDescription = "This widget is a virtual sensor, it simply sends feed data to an asset which you enter from this UI. User enabled."
    } else if (type == "virtual-output") {
        console.log("[2] IDENTIFICATION - Type voutput, topics configured")

        typeDescription = "This will receives actuator commands and responds with an ACK. User input disabled."

        disableUI()

        if (gatewayId != null && deviceId != null) {
            publishTopic = "client." + clientId + ".out.gateway." + gatewayId + ".device." + deviceId + ".asset." + assetId + ".state"
            subscribeTopic = "client." + clientId + ".in.gateway." + gatewayId + ".device." + deviceId + ".asset." + assetId + ".command"
        }
        if (deviceId != null) {
            publishTopic = "client." + clientId + ".out.device." + deviceId + ".asset." + assetId + ".state"
            subscribeTopic = "client." + clientId + ".in.device." + deviceId + ".asset." + assetId + ".command"
        } else {
            publishTopic = "client." + clientId + ".out.asset." + assetId + ".state"
            subscribeTopic = "client." + clientId + ".in.asset." + assetId + ".command"
        }

        subscribeAndAck(source, subscribeTopic)

    } else if (type == "debug") {

        //This is only for the admin, remove this before release
        if (clientId == "att") {
            subscribe("/exchange/inbound/", "#");
            subscribe("/exchange/outbound/", "#");
        } else {
            subscribe(source, "#");
        }

        typeDescription = "This will receives all data that your unique vhost has access to."


    } else {
        console.log("[2] Type not recognized")
        typeConfigured = false;
        return false
    }

    console.log("[3] MANAGEMENT - Nothing to see here")

    if (!type.indexOf("virtual-") > -1 && type != "debug") {
        console.log("[4] TELEMETRY - Subcribing to topic " + subscribeTopic)
        subscribe(source, subscribeTopic);
    }

    if (publishTopic != "") {
        console.log("[4] TELEMETRY - Publish topic configured for " + publishTopic)
    }

    console.log("[5] HISTORIC - Come back later when the API is configured...")

    var summary = "This widget is configured as " + type + "\n\r" + typeDescription;

    log(summary)
    loading(false);
    $('#content').attr('style', 'display: show;');

    return true
}

function disableUI() {
    $('#container *').css('cursor', 'default'); // Don't make the user think they can click the widget
    $('#container *').attr('readonly', 'readonly'); //Make sure it's only displaying data
    $('#container *').prop('disabled', true); // No click events allowed
    $('#container *').css('pointer-events', 'none'); //This doesn't work in old version of IE...
}

function subscribe(source, topic) {

    if (topic != null) {
        telemetry.subscribe(source + topic, function(payload) {
            receiveFeedDataAction(payload);
        });
    } else {
        telemetry.subscribe(source + topic, function(payload) {
            receiveFeedDataAction(payload);
        });
    }
}

function subscribeAndAck(source, topic) {
    telemetry.subscribe(source + topic, function(payload) {
        receiveFeedDataAction(payload);
        pub(payload.body)
    });
}

function pub(message) {

    if (type == "virtual-input" || type == "virtual-output") {
        payload = now() + "|" + message
    } else {
        payload = message
    }

    telemetry.send(source + publishTopic, {
        "content-type": "text/plain"
    }, payload);
}

// This deals with feed data being received
var receiveFeedDataAction = function(message) {


    log("receiveFeedDataAction - " + message);

    var payload = JSON.parse(message.body);

    if (payload.At && (payload.Value != null || payload.Value != undefined)) {
        var unixTimestamp = moment(payload.At, moment.ISO_8601).unix();
        var lastStateTime = moment(payload.At, moment.ISO_8601);
        var timestampText = lastStateTime.fromNow();

        feedData = payload.Value;

        if (validate(feedData)) {

            uiUpdateEvent(feedData, unixTimestamp);

            setTimestamp(timestampText);
        }
    } else {
        log("Data was not from a sensor feed");
        log("Attempt to log as plain text");
        if (validate(message.body)) {
            uiUpdateEvent(message.body, prettynow(now()));
            setTimestamp(prettynow(now()));
        }
    }
}


var isTimeStampRecent = function(timestamp) {
    if (timestamp < (now() - 10000)) {
        return false;
    } else {
        return true;
    }
}

//This deal with user interaction to be sent
var userInputCommandAction = function(input) {
    log("userInputCommandAction - " + input);

    if (validate(input)) { //Validate the payload data with the profile - Currently this is a manual check
        setTimestamp(prettynow(now())); // This will update the browser with the current time, if for example we expect an acknowledgement from the server the feed uiUpdateEvent will be called and update the timestamp again to match what the server has
        pub(input); //Send the data    
    } else {
        log("User interaction data validation failed");
    }
}

function getParam(paramName) {
        var inputUrl = window.location.search.substring(1);
        var params = inputUrl.split('&');
        if (params != 0) {
            for (var i = 0; i < params.length; i++) {
                var param = params[i].split('=');
                if (param[0] == paramName) {
                    if (param[1] != null || param[1] != "undefined") {
                        return param[1];
                    } else {
                        return "";
                    }
                }
            }
        }
    }
    // Timestamp 

function now() {
    var time = Math.round(new Date().getTime() / 1000); //Returns double if we don't round off
    return time;
}

function prettynow(x) {
    return new Date(x * 1000)
}

function prettyhms(x) {
    var timestamp = new Date(x * 1000);

    var h = timestamp.getHours();
    if (h < 10) {
        h = "0" + h;
    }
    var m = timestamp.getMinutes();
    if (m < 10) {
        m = "0" + m;
    }
    var s = timestamp.getSeconds();
    if (s < 10) {
        s = "0" + s;
    }

    return h + ":" + m + ":" + s
}

function setTimestamp(timestampText) {

    ts = document.getElementById('timestamp');

    if (ts != null) {
        ts.innerHTML = timestampText;
        // var cur = prettynow(timestamp)

        // var seconds = 0;
        // seconds = timeSince(cur);
        // var timeAgo = timeSinceText(seconds);

        // if( seconds > 0 ){
        // 	ts.innerHTML = timeAgo;
        // }else if(seconds <= 0) {
        //  ts.innerHTML = "0 second ago";
        // }else if( seconds == 'undefined'){
        // 	ts.innerHTML = "Waiting to Update";
        // }
        // else{
        // 	ts.innerHTML = lastTimestamp;
        // }
    }
}

function timeSince(timeStamp) {
    var now = new Date(),
        secondsPast = (now.getTime() - timeStamp.getTime()) / 1000;
    return parseInt(secondsPast)
}

function timeSinceText(seconds) {
    var timeShow;
    if (seconds < 60 && seconds > 0) {
        if (seconds == 1) {
            timeShow = parseInt(seconds) + ' second ago';
        } else {
            timeShow = parseInt(seconds) + ' seconds ago';
        }

    } else if (seconds < 3600 && seconds >= 60 && seconds > 0) {

        if (seconds == 60) {
            timeShow = parseInt(seconds / 60) + ' minute ago';
        } else {
            timeShow = parseInt(seconds / 60) + ' minutes ago';
        }
    } else if (seconds <= 86400 && seconds >= 3600 && seconds > 60 && seconds > 0) {

        if (seconds == 3600) {
            timeShow = parseInt(seconds / 3600) + ' hour ago';
        } else {
            timeShow = parseInt(seconds / 3600) + ' hours ago';
        }
    } else if (seconds > 86400) {
        if (seconds == 86400) {
            timeShow = parseInt(seconds / 86400) + ' day ago';
        } else {
            timeShow = parseInt(seconds / 86400) + ' days ago';
        }
    } else if (seconds < 0) {
        timeShow = '0 seconds ago';
    }

    return timeShow;
}

// window.setInterval(function(){
// 	if (typeof timestamp !== 'undefined') {
// 		setTimestamp(timestamp);
// 	}
// }, 1000);



function loading(state) {
    if (state == true) {
        $('body').attr('style', 'background:rgba(3,3,3,0.5)');
        $('#loading').attr('style', 'display: show;');
    } else {
        $('body').attr('style', '');
        $('#loading').attr('style', 'display: none;');
        $('#timestamp').attr('style', 'display: show;');
        $('#settings').attr('style', 'display: show;');
    }
}

function setup(state, modal) {
    device = null;
    gateway = null;
    grabSetupParams();
    if (state == true) {

        $('#dialog').remove();
        grabSetupParams();
        $('body').append(modalForm("Setup your widget", "", configHtml));

        $(function() {
            $("#dialog").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                position: ['center'],
                dialogClass: 'setup',
                buttons: {
                    "Create widget": {
                        id: 'create',
                        text: 'Create widget',
                        click: function() {

                            var device = document.getElementById('device').value;
                            var gateway = document.getElementById('gateway').value;
                            loadParams(device, gateway);

                        },
                        "class": "createWidget"
                    },
                    "?": {
                        id: 'help',
                        text: '?',
                        'title': 'For more information visit "http://docs.smartliving.io" ',
                        click: function() {
                            $(this).dialog("close");
                            window.location.replace("http://docs.smartliving.io");
                        },
                        "class": "helpButton"
                    },
                }
            });
            if (modal != 'error') {
                if (profile != null || profile != '') {
                    $(".setup").children(".ui-dialog-titlebar").append("<span id='iconhelp' class='ui-icon ui-icon-script tooltip_display' title='" + profile + "' style='margin-right:7px;'></span>");
                }
                if (description != null || description != '') {
                    $(".setup").children(".ui-dialog-titlebar").append("<span id='iconhelp' class='ui-icon ui-icon-info' title='Description: " + description + "'></span>");
                }
            }



        });

    }
}




var error = function error(state) {
    if (state == true) {

        $('body').append(modalForm("Configuration error", errorHtml, ""));
        $(function() {
            $("#dialog").dialog({

                modal: true,
                draggable: false,
                resizable: false,
                position: ['center'],
                closeOnEscape: false,
                beforeClose: function(event, ui) {
                    return false;
                },
                dialogClass: "noclose",
                buttons: {
                    "?": {
                        id: 'help',
                        text: '?',
                        click: function() {
                            $(this).dialog("close");
                        },
                        "class": "helpButton"
                    },
                    "Edit configuration": {
                        id: 'edit',
                        text: 'Edit configuration',
                        click: function() {
                            $(this).dialog("close");
                            setup(true, 'error');

                        },
                        "class": "edit"
                    },
                }
            });
        });


        // $('#error').attr('style','display: show;');
    } else {
        $('body').attr('style', 'ba');
        $('#error').attr('style', 'display: none;');
        $(function() {
            $("#dialog").close({

                modal: false,
                draggable: false,
                resizable: false,
                position: ['center'],
                show: 'blind',
                hide: 'blind'
            });
        });
    }
}

function modalForm(title, description, html) {
    var input = '<div id="dialog" title="' + title + '">' + description + '</br>' + html + '</div>';
    return input;
}

//UI Debugging helper

var printToLogUI = false;

function log(message) {
    // Any debugging for the widget sent to the console should also be directed to the log div
    console.log(widgetName + ": " + message);
    if (printToLogUI) {
        document.getElementById("log").innerHTML = widgetName + ": " + message;
    }
}


function loadParams(device, gateway) {
    if (device == '' && gateway == '') {
        window.location.search = "?asset=" + $('#asset').val() + "&id=" + $('#id').val() + "&key=" + $('#key').val() + "&type=" + $('#type').val()
    } else if (device != '' && gateway == '') {
        window.location.search = "?asset=" + $('#asset').val() + "&id=" + $('#id').val() + "&key=" + $('#key').val() + "&device=" + $('#device').val() + "&type=" + $('#type').val()
    } else if (device == '' && gateway != '') {
        window.location.search = "?asset=" + $('#asset').val() + "&id=" + $('#id').val() + "&key=" + $('#key').val() + "&gateway=" + $('#gateway').val() + "&type=" + $('#type').val()
    } else {
        window.location.search = "?asset=" + $('#asset').val() + "&id=" + $('#id').val() + "&key=" + $('#key').val() + "&device=" + $('#device').val() + "&gateway=" + $('#gateway').val() + "&type=" + $('#type').val()
    }
}


/* Asset Management */
function getAsset() {
    grabSetupParams();

    var url = "http://api.smartliving.io/asset/" + assetId;

    $.ajax({
        type: "GET",
        dataType: "json",
        url: url,
        headers: {
            'Auth-ClientId': clientId,
            'Auth-ClientKey': clientKey,
            'Content-Type': 'application/json'
        },
        success: function(data) {



            name = data.name;
            description = data.description;
            profile = 'CATEGORY: ' + data.profile.category + '\nDefault: ' + data.profile.default+'\nName: ' + data.profile.name + '\nType: ' + data.profile.type;
            document.title = name;

            if (data.state) {

            	var unixTimestamp = moment(data.state.at, moment.ISO_8601).unix();
                var lastStateTime = moment(data.state.at, moment.ISO_8601);
                var lastStateTimeText = lastStateTime.fromNow();

                lastValue = data.state.value;

                if (validate(lastValue)) {

                    uiUpdateEvent(lastValue, unixTimestamp);
                    setTimestamp(lastStateTimeText);
                }


                // lastValue = data.state.value;
                // lastTimestamp = parseInt((""+getEpochMillis(data.state.at)).slice(0,-3));
                // lastTimestamp = prettynow(lastTimestamp);
                // lastTimestamp = timeSince(lastTimestamp);
                // lastTimestamp = timeSinceText(lastTimestamp);
                // validate(lastValue);
                return lastValue;
            }
            //  setTimestamp(lastTimestamp);
        },
        error: function(xmlHttpRequest, textStatus, errorThrown) {
            log(textStatus, errorThrown);
        }
    });
}
var getEpochMillis = function(dateStr) {
    var r = /^\s*(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)+Z\s*$/,
        m = ("" + dateStr).match(r);
    return (m) ? Date.UTC(m[1], m[2] - 1, m[3], m[4], m[5], m[6]) : undefined;
};


/* Historic Data Management */
function assetHistory(filter) {
    $('#content').attr('style', 'display: none;');
    $('#loading').attr('style', 'display: show;');
    var url = "http://api.smartliving.io/asset/" + assetId + "/states/" + filter;
    var jqXHR = $.ajax({
        async: false,
        type: "GET",
        dataType: "json",
        url: url,
        headers: {
            'Auth-ClientId': clientId,
            'Auth-ClientKey': clientKey,
            'Content-Type': 'application/json'
        },
        success: function(data) {},
        error: function(xmlHttpRequest, textStatus, errorThrown) {
            if (xmlHttpRequest.status == 404) {
                log('Status: ' + xmlHttpRequest.status + ' \n' + 'Sorry no asset found with this ID');
            } else {
                log('Status: ' + xmlHttpRequest.status + ' \n' + xmlHttpRequest.responseJSON.plainErrors);
            }
        }
    });
    if (jqXHR.responseJSON.paging.previous != null) {
        $('#btnPrev').attr('style', 'display: show;');
        prev = jqXHR.responseJSON.paging.previous;
    }
    if (jqXHR.responseJSON.paging.next != null) {
        $('#btnNext').attr('style', 'display: show;');
        next = jqXHR.responseJSON.paging.previous;
    }
    showData(jqXHR.responseJSON)

}

var assetPrevious = function() {
    var url = "http://api.smartliving.io" + prev;
    var jqXHR = $.ajax({
        async: false,
        type: "GET",
        dataType: "json",
        url: url,
        headers: {
            'Auth-ClientId': clientId,
            'Auth-ClientKey': clientKey,
            'Content-Type': 'application/json'
        },
        success: function(data) {},
        error: function(xmlHttpRequest, textStatus, errorThrown) {
            log(textStatus, errorThrown);
        }
    });
    if (jqXHR.responseJSON.paging.prevoius != null) {
        $('#btnPrev').attr('style', 'display: show;');
        prev = jqXHR.responseJSON.paging.previous;
    }
    if (jqXHR.responseJSON.paging.next != null) {
        $('#btnNext').attr('style', 'display: show;');
        next = jqXHR.responseJSON.paging.previous;
    }
    showData(jqXHR.responseJSON)
}



var assetNext = function(filter) {
    var url = "http://api.smartliving.io" + next;
    var jqXHR = $.ajax({
        async: false,
        type: "GET",
        dataType: "json",
        url: url,
        headers: {
            'Auth-ClientId': clientId,
            'Auth-ClientKey': clientKey,
            'Content-Type': 'application/json'
        },
        success: function(data) {},
        error: function(xmlHttpRequest, textStatus, errorThrown) {
            // alert(textStatus, errorThrown);
        }
    });
    if (jqXHR.responseJSON.paging.prevoius != null) {
        $('#btnPrev').attr('style', 'display: show;');
        prev = jqXHR.responseJSON.paging.previous;
    }
    if (jqXHR.responseJSON.paging.next != null) {
        $('#btnNext').attr('style', 'display: show;');
        next = jqXHR.responseJSON.paging.previous;
    }
    showData(jqXHR.responseJSON)
}

function historicDataView() {

    var element = document.getElementById('historicDataConfig');
    if (element.style.display == '') {
        element.style.display = 'none';
        telemetricMode();
        document.getElementById('historicDataOptions').textContent = 'Historic mode'
    } else {
        element.style.display = '';
        document.getElementById('historicDataOptions').textContent = 'Switch to telemetry'
    }
}

var sendMinMax = function() {
    loadControlSettings()
    if (min == null || max == null || min == '' || max == '') {
        var minMax = [null, null];
    } else {
        var minMax = [min, max];
    }
    return minMax;
}
