var http = require("http");
var express = require('express'),
	app = express();

var oneDay = 86400000;
var wwwroot = __dirname;
app.use(express.static(wwwroot, { maxAge: oneDay }));

//app.listen(process.env.PORT || 9000);

var server = app.listen(process.env.PORT || 9000, function() {
    console.log('Listening on port %d', server.address().port);
});

setInterval(function() {
    http.get("http://widget.smartliving.io");
}, 300000);