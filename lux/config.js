/*
	We have to many parts to configure

	receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	userInputCommandAction() - What action must be taken when user input commands are made from the UI

	3 Steps to confiure your widget:
	[1] Specify valadation for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex?)
	[2] Specify how the data is loaded in to the UI element
	[3] Specify how user input is caught and processed
*/

// [1] Set up feed data methods
var validate = function(value){ 
	// Put in a reg ex, comaparison, or compare against the resource profile
	if(value > -1 || value < 1025){
		return true;
	}else{
		 log("This is a knob widget which only allows integer values between 0 and 1024");
		return false;
	}
}

// [2] How do you want to update the UI element
var uiUpdateEvent = function(feedData){
	 //Add your custom logic to update the user interface based from the payload data
		$('#knobWidget').val(feedData).trigger('change'); 
//
//      "meta": {
//        "value":"illuminance",
//        "key":"Surface illuminated by"
//      },
 
luxMap = {
		"Moonless, overcast night sky (starlight)":0.0001,
		"Moonless clear night sky with airglow":0.002,
		"Full moon on a clear night":1.0,
		"Dark limit of civil twilight under a clear sky":3.4,
		"Family living room lights":50,
		"Office building hallway/toilet lighting":80,
		"Very dark overcast day":100,
		"Overcast day":250,
		"Sunrise or sunset on a clear day":400,
		"Office lighting":500,
		"Clear mid day":800,
		"typical TV studio lighting":1000,
		"Full daylight (not direct sun)":25000,
		"Direct sunlight":100000,
		"You're probably on fire right now":999999
	};


message ="Really really dark";

$.each(luxMap, function(key,value){
	if(parseInt(feedData,10) > parseInt(value,10)){
			message = key;
		}
});

$('#context').html(message);

}
//[3] Specify how user input is caught and processed
	//There options here
	//   Set up a listener - E.g   $('#myInput').change(function() {};
	//   Set this as an onclick function on the DOM object - E.go onclick='userInputCommandAction(this)'
	//   Call this function from within widgets custom code
var catchInputEvent = function(){
	$("#knobWidget").knob({
		change : function (input) {
			userInputCommandAction(input);
		}
	});
}


//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	//E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
	
	// Primarily deals with drawing the knob and making it responsive to page load (Not responsive by default)
	window.onresize = function(){ 
		location.reload(); 
	}

	var knobWidthHeight,windowObj = $(window);
	if(windowObj.height() > windowObj.width()){
		knobWidthHeight = Math.round(windowObj.width()*0.80);
	}else{
		knobWidthHeight = Math.round(windowObj.height()*0.80);
	}

	$('#knobWidget').attr('data-width',knobWidthHeight).attr('data-height',knobWidthHeight);
	catchInputEvent();
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);