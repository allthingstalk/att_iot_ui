var validate = function(value){ 
   return true; //No validation!
}

var setFeed = [];

var optionsAnimation = {
	scaleSteps : 10,
	scaleStepWidth : 10,
	scaleStartValue : 0,
	responsive: true,
}

var optionsNoAnimation = {
	animation : false,
	scaleOverride : true,
	scaleSteps : 10,
	scaleStepWidth : 10,
	scaleStartValue : 0,
	
}

//"#1a6deb"
	var barChartData = {
		labels : ["Time"],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [setFeed]
			}
		]

	}
	var ctx = document.getElementById("canvas").getContext("2d");
	var myBar = new Chart(ctx)
	myBar.Bar(barChartData, optionsAnimation);

var uiUpdateEvent = function(feedData, timestamp){
	
	var timestamp = (timestamp * 1000) + 8000;
	timestamp = new Date(timestamp);
	timestamp = timestamp.toLocaleTimeString();
	feedData = Math.round(feedData);
	
	barChartData = {
		labels : ["Time"],
		datasets : [
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [feedData]
			}
		]

	}
	console.log("feedData ----- " + feedData);

	myBar.Bar(barChartData, optionsNoAnimation);
	var ts = document.getElementById('timestamp')
	if(ts!=null){
		ts.innerHTML = timestamp;
	}
}


//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){}

init(widget);