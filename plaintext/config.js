/*

 
	3 Steps to confiure your widget
	- [1] Specify valadation or clean data for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex, do double need to be converted?)
	- [2] Specify how the data is loaded in to the UI element (How is the DOM updated? Plain old JS, jQuery, web components?)
	- [3] Specify how user input is caught and processed (Probably going to use an onClick event or other eventListeners)

	att_iot_widget.js - receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	att_iot_widget.js - userInputCommandAction() - What action must be taken when user input commands are made from the UI

*/

// [1] Set up feed data methods
var validate = function(value){ 
 // Put in a reg ex, comaparison, or compare against the resource profile
	document.getElementById("payload").innerHTML = value;
	 return true; //No validation!
}

// [2] How do you want to update the UI element
var uiUpdateEvent = function(feedData){

	console.log("ui event"+feedData)

	 //Add your custom logic to update the user interface based from the payload data
	document.getElementById("payload").innerHTML = feedData ;
}

//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	//E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);