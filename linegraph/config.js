

var min = null;
var max = null;

var minMax = sendMinMax();
	min = minMax[0];
	max = minMax[1];

var historicDataFlag = false;
var historyBucket = {'history': []};
var prev = null;
var next = null;

var validate = function(value){ 
	if(isNumber(value)){
		return true;
	}else{
		return false;
	}
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

var setFeed 	= [];
var labels 		= [];

if(min == null || max == null)
{
	var optionsNoAnimation = 
	{
		animation : false,
		responsive: true
	}
}else
{
	max = max/10;
	var optionsNoAnimation = 
	{
		animation : false,
		scaleOverride: true,// Number - The number of steps in a hard coded scale
		scaleSteps: 10,
		// Number - The value jump in the hard coded scale
		scaleStepWidth: parseInt(max),
		// Number - The scale starting value
		scaleStartValue: parseInt(min),
		// String - Colour of the scale line
		scaleLineColor: "rgba(0,0,0.1)",
		// Number - Pixel width of the scale line
		scaleLineWidth: 1,
		// Boolean - Whether to show labels on the scale
		scaleShowLabels: true,
		// Interpolated JS string - can access value
		scaleLabel: "<%=value%>",
		// Boolean - Whether the scale should stick to integers, and not show any floats even if drawing space is there
		scaleIntegersOnly: true,
		// Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
		scaleBeginAtZero: false,
		// String - Scale label font declaration for the scale label
		scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
		// Number - Scale label font size in pixels
		scaleFontSize: 12,
		// String - Scale label font weight style
		scaleFontStyle: "normal",
		// String - Scale label font colour
		scaleFontColor: "#666",
		// Boolean - whether or not the chart should be responsive and resize when the browser does.
		responsive: true,
	}
}


var lineChartData = {
	labels : labels,
	datasets : [
	{
		fillColor : "rgba(151,187,205,0.0)",
		strokeColor : "#1a6deb",
		pointColor : "#1a6deb",
		pointStrokeColor : "#fff",
		pointHighlightFill : "#fff",
		pointHighlightStroke : "rgba(151,187,205,1)",
		data : setFeed
	}
	]
};

var ctx;
var myNewChart;

var uiUpdateEvent = function(feedData, timestamp){
	if (historicDataFlag == false)
	{
		var x = document.getElementById("content").readOnly;
		if(isTimeStampRecent(timestamp)){
			timestamp = prettyhms(timestamp);
		}else{
			timestamp = prettyhms(now());
		}
		setFeed.push(feedData);
		labels.push(timestamp);
		//console.log("feedData ----- " + setFeed + " " + setFeed.length);
		//console.log("TimeStamp ----- " + labels + " " + labels.length);

		if(setFeed.length > 10)
		{
			setFeed.shift();
			labels.shift();
		//	console.log("feedData ----- " + setFeed + " " + setFeed.length);
	//		console.log("TimeStamp ----- " + labels + " " + labels.length);
		}
		if(setFeed.length > 2)
		{	
			$('#updatecheck').attr('style','display: none;');
			$('#timestamp').attr('style','display: show;');
			resetCanvas();
			ctx = $("#canvas").get(0).getContext("2d");
			myNewChart = new Chart(ctx).Line(lineChartData, optionsNoAnimation);
		}
	}
}

//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	$('#updatecheck').attr('style','display: show;');
	// document.getElementById("historicDataConfig").style.display = "none";
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);

var resetCanvas = function(){
  $('#canvas').remove(); // this is my <canvas> element
  $('#widgetcontainer').append('<canvas id="canvas"><canvas>');
}

var showData = function(data){
	historyBucket = [];
	historicDataFlag = true;
	historyBucket = data;
	setFeed = [];
	labels = [];
	resetCanvas();
	clearCanvas();

	if (historyBucket.data.length == 0)
	{
		$('#loading').attr('style','display: none;');
		document.getElementById('updatecheck').innerHTML = 'No Data Returned';
		alert("No data found in the time range")
	}
	else{
		for(i=0; i<historyBucket.data.length;i++)
		{
			timestamp = historyBucket.data[i].at;
			timestamptext = timestamp.split('T')[1];
			timestamptext = timestamptext.replace('Z','');
			setFeed.push(historyBucket.data[i].data);
			labels.push(timestamptext);
		}
		$('#loading').attr('style','display: none;');
		$('#content').attr('style','display: show;');
		resetCanvas();
		ctx = $("#canvas").get(0).getContext("2d");
		myNewChart = new Chart(ctx).Line(lineChartData, optionsNoAnimation);
	}

	
}

var telemetricMode = function(){
	setFeed = [];
	labels = [];
	historyBucket = [];
	resetCanvas();
	$('#updatecheck').attr('style','display: show;');
	$('#timestamp').attr('style','display: none;');
	clearCanvas();
	historicDataFlag = false;
}

var clearCanvas = function(){
	lineChartData = [];
	lineChartData = {
	labels : labels,
	datasets : [
			{
				fillColor : "rgba(151,187,205,0.0)",
				strokeColor : "#1a6deb",
				pointColor : "#1a6deb",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : setFeed
			}
		]
	};
}