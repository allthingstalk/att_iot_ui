var validate = function(value){ 
   return true; //No validation required!
}

var uiUpdateEvent = function(feedData){
	 $.growl(feedData, {
		   type: "info"
		 });  
}

var widget = function (){
  //E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);