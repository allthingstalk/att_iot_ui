/*

	We have to many parts to configure

	receiveFeedDataAction() - What action must be taken when we receive feed data from the broker
	userInputCommandAction() - What action must be taken when user input commands are made from the UI

	3 Steps to confiure your widget
	- [1] Specify valadation for the widget (Is it boolean, does it have an integer range, does it have multiple values, does it require regex?)
	- [2] Specify how the data is loaded in to the UI element
	- [3] Specify how user input is caught and processed
*/

// [1] Set up feed data methods
var validate = function(value){ 
 // Put in a reg ex, comaparison, or compare against the resource profile
	 return true; //No validation!
}

// [2] How do you want to update the UI element
var uiUpdateEvent = function(feedData){
	 speak(config, feedData);
}
//Any special action required once the connection is set up and the widget is ready to be displayed on the screen
var widget = function (){
	//E.g If a widget needs to be drawn you should do it here once you're sure the connection and everything else is ready
}

//Let's do the hardwork and connect the client to the broker and load up the UI
init(widget);

 function speak(config, text){
			config.text = text;
			window.speechSynthesis.speak(config);
	 }

	function setup(){
		// Test browser support
			if (window.SpeechSynthesisUtterance === undefined) {
				alert("Looks like you're running an old browers no IoT Goodies for you :-(")
			} else {
				var utterance = new SpeechSynthesisUtterance();
				utterance.text = "";
				utterance.voice = "Google UK English Female";
				utterance.lang = "en-US";
				utterance.rate = 9;
				utterance.pitch = 9;

						utterance.onstart = function() {
							console.log('Speaker started');
						};
						utterance.onend = function() {
							console.log('Speaker ended');
						};

						return utterance;
			 }
	}

		var config = setup();
